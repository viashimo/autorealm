#include <gui/renderwindow.h>

#include "singledrawer.h"
#include "renderer.h"

SingleDrawer::SingleDrawer(RenderWindow *window, std::unique_ptr<Renderer> r, Render::TagList const &tags)
:Drawer(window, std::move(r), tags)
{
}

SingleDrawer::SingleDrawer(SingleDrawer const& other)
:Drawer(other)
{
}

SingleDrawer::~SingleDrawer(void)throw()
{
	removeEventManager();
}

void SingleDrawer::installEventManager(void) throw()
{
	m_target->Bind(wxEVT_LEFT_DOWN, &SingleDrawer::firstPoint, this);
}

void SingleDrawer::removeEventManager(void) throw()
{
	if(!m_shape.empty())
		m_target->Unbind(wxEVT_LEFT_DOWN, &SingleDrawer::firstPoint, this);
	else
	{
		m_target->Unbind(wxEVT_MOTION, &SingleDrawer::moveMouse, this);
		m_target->Unbind(wxEVT_LEFT_DOWN, &SingleDrawer::secondPoint, this);
		m_target->Unbind(wxEVT_RIGHT_DOWN, &SingleDrawer::finalizeShape, this);
	}
}

void SingleDrawer::finalizeShape(wxMouseEvent &event)
{
	m_shape.pop();
	m_target->push_back(m_shape);
	render();
	m_shape.clear();

	removeEventManager();
	installEventManager();
}

void SingleDrawer::firstPoint(wxMouseEvent &event)
{
	removeEventManager();
	createShape();
	addVertex(Render::Point(event.GetX(),event.GetY(),0));
	addPoint(event);

	m_target->Bind(wxEVT_LEFT_DOWN, &SingleDrawer::secondPoint, this);
	m_target->Bind(wxEVT_MOTION, &Drawer::moveMouse, this);//when a shape have a point, draw a line between it and the cursor
	m_target->Bind(wxEVT_RIGHT_DOWN, &SingleDrawer::finalizeShape, this);
}

void SingleDrawer::secondPoint(wxMouseEvent &event)
{
	addPoint(event);
	finalizeShape(event);
}
