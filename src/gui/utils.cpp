/**********************************************************************************
 *autorealm - A vectorized graphic editor to create maps, mostly for RPG games    *
 *Copyright (C) 2012-2013 Morel Bérenger                                          *
 *                                                                                *
 *This file is part of autorealm.                                                 *
 *                                                                                *
 *    autorealm is free software: you can redistribute it and/or modify           *
 *    it under the terms of the GNU Lesser General Public License as published by *
 *    the Free Software Foundation, either version 3 of the License, or           *
 *    (at your option) any later version.                                         *
 *                                                                                *
 *    autorealm is distributed in the hope that it will be useful,                *
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 *    GNU Lesser General Public License for more details.                         *
 *                                                                                *
 *    You should have received a copy of the GNU Lesser General Public License    *
 *    along with autorealm.  If not, see <http://www.gnu.org/licenses/>.          *
 **********************************************************************************/

#include "utils.h"

#include <wx/msgdlg.h>
#include <boost/filesystem.hpp>

namespace fs=boost::filesystem;

std::string getUserConfigDir(void)
{
	///\todo find a better solution to follow freeDesktop.org's recommmandations
	std::string homepath(getenv(HOME));///\note flawfinder say I should use getenv with care

	const fs::path usualUserConfig(homepath + "/.autorealm/config");
	if(fs::exists(usualUserConfig))
		return usualUserConfig.string();

	const fs::path xdgUserConfig(homepath + "/.config/autorealm/config");
	if(fs::exists(xdgUserConfig))
		return xdgUserConfig.string();
	throw(std::runtime_error("Unable to find configuration in "+usualUserConfig.string()+" or "+xdgUserConfig.string()));
}

std::string getSystemConfigDir(void)
{
	#ifdef WIN32
		throw std::logic_error("System configuration folder detection unimplemented on Windows");
	#endif

	const fs::path systemConfFile= SYSTEM_CONFIG_LOCATION;
	if(fs::exists(systemConfFile))
		return systemConfFile.string();
	throw std::runtime_error("Unable to find system-wide configuration file:\n\""+systemConfFile.string()+"\"");
}

std::string nonFatalExtractString(std::string (*callback)(void))
{
	try
	{
		return callback();
	}catch(std::exception &e)
	{
		wxMessageBox(std::string("An exception has occured.\nAutoREALM might recover from it, or might crash latter. Here is the error:\n")+e.what(), "Non-fatal exception");
	}
	return std::string();
}
