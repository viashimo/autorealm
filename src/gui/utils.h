/**********************************************************************************
 *autorealm - A vectorized graphic editor to create maps, mostly for RPG games    *
 *Copyright (C) 2012-2013 Morel Bérenger                                          *
 *                                                                                *
 *This file is part of autorealm.                                                 *
 *                                                                                *
 *    autorealm is free software: you can redistribute it and/or modify           *
 *    it under the terms of the GNU Lesser General Public License as published by *
 *    the Free Software Foundation, either version 3 of the License, or           *
 *    (at your option) any later version.                                         *
 *                                                                                *
 *    autorealm is distributed in the hope that it will be useful,                *
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 *    GNU Lesser General Public License for more details.                         *
 *                                                                                *
 *    You should have received a copy of the GNU Lesser General Public License    *
 *    along with autorealm.  If not, see <http://www.gnu.org/licenses/>.          *
 **********************************************************************************/

#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>
#include <string>

#include <wx/bitmap.h>
#include <boost/filesystem.hpp>
#include <Pluma/Pluma.hpp>

#include <gui/appconfig.h>
#include <renderEngine/taglist.h>

#ifdef WIN32
#  define HOME "HOMEPATH" ///\todo check that it should not be %APPDATA% instead of %HOMEPATH%
#  ifndef SYSTEM_CONFIG_LOCATION 
#    define SYSTEM_CONFIG_LOCATION "???"
#  endif
#  ifndef PLUGIN_EXTENSION
#    define PLUGIN_EXTENSION ".dll"
#  endif
#else
#  define HOME "HOME"
#  ifndef SYSTEM_CONFIG_LOCATION 
#    define SYSTEM_CONFIG_LOCATION "/usr/local/etc/autorealm/config"
#  endif
#  ifndef PLUGIN_EXTENSION
#    define PLUGIN_EXTENSION ".so"
#  endif
#endif

/**
 * \brief returns provider corresponding to given plug-in
 * If the plug-in already have a provider, simply returns it.
 * \pre location+'/'+pluginName must point to a valid plug-in
 * \post the desired provider is returned
 * \throw runtime_error if the file was not found
 * \throw runtime_error if the provider has not been created
 * \todo that code should be contributed to pluma to have better performances
 * \todo PlumaLack#1 implement a Provider::getProvider(std::string const &plugName) in pluma to remove dumb code here
 */
template <class T>
T *getProvider(pluma::Pluma &plumConf, std::string const &location, std::string const &pluginName)
{
	std::string fullName(location+"/"+pluginName+PLUGIN_EXTENSION);
	if(!boost::filesystem::exists(boost::filesystem::path(fullName)))
		throw std::runtime_error("Plug-in "+fullName+" was not found.");

	std::vector<T *> prevProviders, actualProviders;
	plumConf.getProviders(prevProviders);

	if(plumConf.load(location, pluginName))
	{
		// register loaded provider
		decltype(actualProviders.size()) i = 0;
		plumConf.getProviders(actualProviders);

		//Locate the provider newly loaded
		while(i < actualProviders.size() && prevProviders.end() != std::find(prevProviders.begin(), prevProviders.end(), actualProviders[i]))
			++i;
		// if it was found, return it.
		if(i <= actualProviders.size())
			return actualProviders[i];
	}

	throw std::runtime_error("Unable to create a valid provider from file "+fullName);
}

/**
 * \brief return autorealm's general configuration file for the current user
 * Autorealm checks for files ~/.autorealm/config and ~/.config/autorealm/config in that exact order.
 * \throw runtime_error if file was not found
 */
std::string getUserConfigDir(void);

/**
 * \brief return autorealm's general configuration file for the system
 * \throw runtime_error if file was not found
 * \throw logic_error if used on windows
 * \todo implement windows mechanisms
 * \pre SYSTEM_CONFIG_LOCATION is defined on an existing file
 */
std::string getSystemConfigDir(void);

/**
 * \brief show a warning message box if the given function throws an exception, or returns the string it should have returned
 * \return null string or callback's result
 */
std::string nonFatalExtractString(std::string (*callback)(void));

#endif // UTILS_H
