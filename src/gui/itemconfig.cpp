/**********************************************************************************
 *autorealm - A vectorized graphic editor to create maps, mostly for RPG games    *
 *Copyright (C) 2012-2013 Morel Bérenger                                          *
 *                                                                                *
 *This file is part of autorealm.                                                 *
 *                                                                                *
 *    autorealm is free software: you can redistribute it and/or modify           *
 *    it under the terms of the GNU Lesser General Public License as published by *
 *    the Free Software Foundation, either version 3 of the License, or           *
 *    (at your option) any later version.                                         *
 *                                                                                *
 *    autorealm is distributed in the hope that it will be useful,                *
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 *    GNU Lesser General Public License for more details.                         *
 *                                                                                *
 *    You should have received a copy of the GNU Lesser General Public License    *
 *    along with autorealm.  If not, see <http://www.gnu.org/licenses/>.          *
 **********************************************************************************/

#include "itemconfig.h"
#include "id.h"

ItemConfig::ItemConfig(fs::path const& rootDir)
:m_path(rootDir), m_odFile("File")
{
	if(fs::is_directory(rootDir))
		m_path=fs::path(m_path.string()+"/"+m_path.filename().string());
	
	if(!fs::exists(m_path))
		throw std::runtime_error("Configuration files "+m_path.string()+"does not exists.");

	m_odFile.add_options()
		("description", po::value<std::string>(&m_desc))
		("plugin",po::value<std::string>(&m_plugin))
		("kind",po::value<ItemKind>(&m_kind)->multitoken())
		;
}

ItemConfig::ItemConfig(ItemConfig && other)
:m_path(std::move(other.m_path))
,m_desc(std::move(other.m_desc))
,m_plugin(std::move(other.m_plugin))
,m_kind(std::move(other.m_kind))
{
}

ItemConfig::ItemConfig(ItemConfig const& other)
:m_path(other.m_path)
,m_desc(other.m_desc)
,m_plugin(other.m_plugin)
,m_kind(other.m_kind)
{
}

std::string ItemConfig::desc(void)const
{
	return m_desc;
}

std::string ItemConfig::plugin(void)const
{
	return m_plugin;
}

ItemKind  ItemConfig::kind(void)const
{
	return m_kind;
}

uint16_t ItemConfig::id(void)const
{
	return m_id;
}

void ItemConfig::setId(uint16_t id)
{
	m_id=id;
}
